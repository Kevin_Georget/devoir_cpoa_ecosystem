import java.util.ArrayList;

public abstract class Animal implements IEvent {
  protected String nom;
  protected Integer energie;
  protected String sexe;
  protected Integer energieMax;
  protected Integer age;
  //Comportement :
  private Iage age;
  private Relation relation;
  private RegimeAlimentaire regime;
  //Etat
  private Automate controlleur;
  //Ce qu'il voit
  private ArrayList<Terrain> listeTerrain;

  // Methodes
  public Animal(String nom, Integer energie, Integer energieMax, String sexe, Integer age){
    this.nom = nom;
    this.energie = energie;
    this.energieMax = energieMax;
    this.sexe = sexe;
    this.age = age;
  }
  public void vivre() {}
}